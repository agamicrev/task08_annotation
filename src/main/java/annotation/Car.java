package annotation;

import java.lang.annotation.Annotation;

@MyAnnotation(model = "Toyota", id = 5)
public class Car {

  private String country;
  private int speed;

  Car(String country, int speed) {
    this.country = country;
    this.speed = speed;
  }

  public void printFields() {
    Class aClass = Car.class;
    Annotation[] annotations = aClass.getAnnotations();

    for (Annotation annotation : annotations) {
      MyAnnotation myAnnotation = (MyAnnotation) annotation;
      System.out.println("Model: " + myAnnotation.model());
      System.out.println("id: " + myAnnotation.id());
    }
  }

  @Message(id = 1)
  public void method1(String name) {
    System.out.println("Params: " + name);
  }

  @Message(id = 2)
  public void method2(int count, String name) {
    System.out.println("Params: " + count + " " + name);
  }

  @Message(id = 3)
  public String method3(String... args) {
    StringBuilder sb = new StringBuilder();
    for (String s : args) {
      sb.append(s);
    }
    return sb.toString();
  }

}
