package annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class App {

  public static void main( String[] args ) throws Exception {

    // Printing fields of the annotation
    Car car = new Car("Japan", 250);
    car.printFields();

    // Print annotation value into console
    Annotation annos[] = car.getClass().getAnnotations();
    System.out.println("\n**** All annotations for class Car ****");
    for (Annotation a : annos)
      System.out.println(a);

    System.out.println();

    // Getting methods through reflection
    Class<Car> aClass = Car.class;

    Method method = aClass.getMethod("method1", String.class);
    Method method1 = aClass.getMethod("method2", int.class, String.class);
    Method method2 = aClass.getMethod("method3", String[].class);

    String[] ar = new String[] {"Andrii ", "Vertsimaha"};

    // Invoke three methods with different parameters and return types
    System.out.println("**** Invoke methods ****");
    if(method.isAnnotationPresent(Message.class)){
      method.invoke(car , new String("It's method 1"));
      method1.invoke(car , 12, "It's method 2");
      String name = (String) method2.invoke(car, new Object[]{ar});
      System.out.println(name);
    }

    OwnClass.helperClass(car);

  }
}
